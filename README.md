# Librairie pour retrouver les jours fériés par année

Les jours fériés mobiles sont calculés d'après la date de Pâques.

Un fichier JSON permet de paramétrer les jours fériés en fonction d'un pays / d'une région.
Un fichier JSON de base est configuré par défaut. Il correspond aux jours fériés français, hors particularités régionales.

Les fichiers JSON décrivant les jours fériés doivent être ajoutés dans un répertoire holidays-conf de l'application.
Ce répertoire holidays-conf est lui même situé dans le répertoire des sources Java.

Nom du fichier de JSON : holidays-PP[-RRR].json
Où :
- PP est le nom du pays sur deux lettres conformément à ISO 3166-1 alpha 2
- RRR est le code région (cf. ISO 3166-2)

Exemples :
- holidays-GB.json
- holidays-FR-A.json pour jours fériés en france avec spécifités de la région Alsace

Structure du fichier JSON

Propriétés
- fixed : tableau des jours de congé fixes sous forme de tableau JSON
- mobile : tableau des jours de congés variables sous forme de tableau JSON

Propritétés JSON d'un jour de congé fixe
- name : nom du jour de congé
- day : numéro du jour dans le mois
- month : numéro du mois (de 1 à 12)

Exemple :
```json
{"name": "Jour de l'an", "day": 1, "month": 1}
```

Propriétés JSON d'un jour de congé variable
- name : nom du jour de congé
- base : base de calcul du jour de congé variable - actuellement seulement Pâque (easter)
- offset : offset du jour de congé par rapport à la base

Exemple :
```json
{"name": "Lundi de Pâques", "base":"easter", "offset":1}
```

Utilisation de base :
Un builder permet de récupérer les jours de congé pour une année, pour un pays, pour une région.

Exemple :
```java
		PublicHolidays holidays = new PublicHolidaysBuilder()
						.setCountryCode("GB")
						.buid();
		holidays.forEach(System.out::println);
```

Dépendances MAVEN :
```maven
<dependency>
  <groupId>org.antislashn</groupId>
  <artifactId>public-holidays</artifactId>
  <version>1.0.5</version>
</dependency>
```

VERSIONS
- 0.0.0	: version initiale avec fichiers properties
- 0.1.0 : fichiers json au lieu de fichiers properties
- 0.1.1 : refactoring
- 1.0.0 : prise en compte des classes développées par l'utilisateur de la librairie
- 1.0.1 : refactoring
- 1.0.2 : refactoring
- 1.0.3 : première version en production
- 1.0.4 : refactoring
- 1.0.5 : changements dans le pom.xml, publication dans le repo MAVEN (http://repo.maven.apache.org/maven2)

UTILISATION
- une fois compilé, le jar peut être utilisé dans votre application
- votre application doit comporter un répertoire ```holidays-conf```
- ce répertoire peut inclure :
- les fichiers JSON décrivant les jours fériés
- le fichier bases.properties qui déclare les classes implémentant l'interface ```BaseDateCompute```
* chaque entrée est sous la forme ```<base>=<nomcomplet de la classe>```
```java
easter=org.antislashn.holidays.mobile.EasterCompute
```


LICENCE : MPL 2.0 
https://www.mozilla.org/en-US/MPL/2.0/