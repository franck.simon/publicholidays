package org.antislashn.holidays.mobile;

import java.util.Calendar;
import java.util.GregorianCalendar;


/**
 * Compute Easter day
 * <br>
 * Butcher-Meeus method
 * 
 * @author Franck SIMON
 *
 */
public class EasterCompute implements BaseDateCompute {
	
	@Override
	public Calendar getHolidayDate(int year) {
		int n = year % 19;
		int c = year / 100;
		int u = year % 100;
		
		int s = c / 4;
		int t = c % 4;
		
		int p = (c + 8) / 25;
		int q = (c - p + 1) / 3;
		
		int e = (19*n + c - s - q + 15) % 30;
		int b = u / 4;
		int d = u % 4;
		int L = (2 * t + 2 * b - e - d + 32) % 7;
		int h = (n + 11 * e + 22 * L) / 451;
		int m = (e + L - 7*h + 114) / 31;
		int j = (e + L - 7*h + 114) % 31;
		
		Calendar easter = new GregorianCalendar(year,m-1,j+1);
		
		return easter;
	}
	

	@Override
	public Calendar getHolidayDate(int year, int offset) {
		Calendar c = getHolidayDate(year);
		c.add(Calendar.DAY_OF_MONTH, offset);
		return c;
	}
	
	

}
