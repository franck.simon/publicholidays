package org.antislashn.holidays.mobile;

import java.util.Calendar;

/**
 * Simple interface for compute a base for mobile public holidays
 * <br>
 * A mobile holiday is related to this base
 * 
 * @author Franck SIMON
 *
 */
public interface BaseDateCompute {

	/**
	 * 
	 * @param year : year for compute de base
	 * @return the date of the mobile public holiday
	 * 
	 * cf. https://fr.wikipedia.org/wiki/Calcul_de_la_date_de_P%C3%A2ques
	 */
	Calendar getHolidayDate(int year);

	/**
	 * 
	 * @param year : year for compute de base
	 * @param offset : offset related to the date of the mobile public holiday
	 * @return the date of the mobile public holiday
	 */
	Calendar getHolidayDate(int year, int offset);

}