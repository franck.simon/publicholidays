package org.antislashn.holidays.mobile;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import org.antislashn.holidays.PublicHolidayException;

/**
 * Simple factory. <br>
 * Returns a base for compute mobile holidays
 * 
 * @author Franck SIMON
 *
 */
public class BaseDateComputeFactory {
	private static final Logger LOG = Logger.getLogger(BaseDateComputeFactory.class.getName());
	private static final String OUTER_POPERTIES = "/holidays-conf/bases.properties";
	private static BaseDateComputeFactory instance;

	private Map<String, String> clazzes = new HashMap<String, String>();

	static {
		instance = new BaseDateComputeFactory();
	}

	private BaseDateComputeFactory() {
	}

	public static BaseDateComputeFactory getInstance() {
		return instance;
	}

	private void init() throws PublicHolidayException {
		// Recherche dans le fichier de configuration du jar
		String fileName = OUTER_POPERTIES;
		Properties props = new Properties();
		props.put("easter", "org.antislashn.holidays.mobile.EasterCompute");

		try {
			LOG.info("Classes loaded from " + fileName);
			InputStream in = getClass().getResourceAsStream(fileName);
			if (in != null) {
				props.load(in);
				LOG.info("Classes loaded from " + fileName);
			}
		} catch (IOException e) {
			LOG.severe("File not found : " + fileName);
			throw new PublicHolidayException(e);
		}

		for (Object key : props.keySet()) {
			String val = props.getProperty((String) key);
			clazzes.put((String) key, val);
			LOG.info("Classe found : "+key + " => " + val);
		}
	}

	public BaseDateCompute create(String base) throws PublicHolidayException {
		BaseDateCompute compute = null;
		if (clazzes.isEmpty()) {
			init();
		}
		for (String key : clazzes.keySet()) {
			if (key.equals(base)) {
				try {
					compute = (BaseDateCompute) Class.forName(clazzes.get(key)).newInstance();
				} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
					LOG.severe("Classe not found for : " + base);
					throw new PublicHolidayException(e);
				}
				break;
			}
		}

		return compute;
	}
}
