package org.antislashn.holidays;
/**
 * 
 * @author Franck SIMON
 *
 */
public class PublicHolidayException extends Exception {

	public PublicHolidayException() {}

	public PublicHolidayException(String message) {
		super(message);
	}

	public PublicHolidayException(Throwable cause) {
		super(cause);
	}

	public PublicHolidayException(String message, Throwable cause) {
		super(message, cause);
	}

	public PublicHolidayException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
