package org.antislashn.holidays;

import java.io.Serializable;
import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Locale;
/**
 * A public holiday
 * <br>
 * Two properties :
 * <ul>
 * 	<li>date : Calendar</li>
 * 	<li>name : the name of the public holiday</li>
 * </ul>
 * 
 * @author Franck SIMON
 *
 */
public class PublicHoliday implements Serializable {
	private Calendar date;
	private String name;
	private static String[] weekdays;
	
	static {
		DateFormatSymbols dfs = new DateFormatSymbols();
		weekdays = dfs.getWeekdays();		
	}
	
	public PublicHoliday() {}

	public PublicHoliday(Calendar date, String name) {
		this.date = (Calendar) date.clone();
		this.setName(new String(name));
	}

	public Calendar getDate() {
		return (Calendar) date.clone();
	}

	public void setDate(Calendar date) {
		this.date = (Calendar) date.clone();
	}

	public String getName() {
		return new String(name);
	}

	public void setName(String name) {
		this.name = new String(name);
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(String.format("%-10s",weekdays[date.get(Calendar.DAY_OF_WEEK)]))
			.append(' ')
			.append(String.format("%02d/%02d/%04d", date.get(Calendar.DAY_OF_MONTH), date.get(Calendar.MONTH)+1,date.get(Calendar.YEAR)))
			.append(" : ")
			.append(name);	
		return sb.toString();
	}

}
