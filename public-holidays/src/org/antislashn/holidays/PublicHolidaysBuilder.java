package org.antislashn.holidays;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.logging.Logger;

import org.antislashn.holidays.mobile.BaseDateCompute;
import org.antislashn.holidays.mobile.BaseDateComputeFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Builder for PublicHolidays <br>
 * The builder accepts :
 * <ul>
 * <li>the year : default the current year</li>
 * <li>the country code : default FR</li>
 * <li>the country subdivision code</li>
 * </ul>
 * 
 * @author Franck SIMON
 *
 */
public class PublicHolidaysBuilder {
	private static final Logger LOG = Logger.getLogger(PublicHolidaysBuilder.class.getName());
	private static final String FILE_NAME = "holidays";
	private static final String OUTER_FOLDER = "/holidays-conf/";
	private static final String INNER_FOLDER = "conf/";
	private static final String DEFAULT_JSON_FILE = "holidays.json";
	private static final String FILE_EXTENSION = "json";

	private boolean yearSetted = false;
	private int year = 0;
	private String countryCode = null;
	private String countrySubdivisionCode = null;

	/**
	 * 
	 * @param year : year to compute the public holidays
	 * @return the builder
	 */
	public PublicHolidaysBuilder setYear(int year) {
		this.year = year;
		this.yearSetted = true;
		return this;
	}

	/**
	 * 
	 * @param countryCode cf. ISO 3166-1 alpha 2
	 * @return the builder
	 */
	public PublicHolidaysBuilder setCountryCode(String countryCode) {
		this.countryCode = countryCode;
		return this;
	}

	/**
	 * 
	 * @param countrySubdivisionCode cf. ISO 3166-2
	 * @return the builder
	 */
	public PublicHolidaysBuilder setCountrySubdivisionCode(String countrySubdivisionCode) {
		this.countrySubdivisionCode = countrySubdivisionCode;
		return this;
	}

	/**
	 * 
	 * @return public holidays collection
	 * @throws PublicHolidayException file not found
	 */
	public PublicHolidays build() throws PublicHolidayException {
		initialize();
		PublicHolidays publicHolidays = new PublicHolidays();
		Reader reader = null;
		try {
			// public holidays for COUNTRY
			reader = getReaderForCountryFile();
			initializePublicHolidays(publicHolidays, reader);
			reader.close();
			
			// public holidays for COUNTRY SUBDIVISION
			reader = getReaderForCountrySubdivisionFile();
			if (reader != null) {
				initializePublicHolidays(publicHolidays, reader);
			}
		} catch (Exception e) {
			throw new PublicHolidayException(e);
		} finally {
			try {
				if(reader != null)
					reader.close();
			} catch (IOException e) {
				throw new PublicHolidayException(e);
			}
		}

		return publicHolidays;
	}

	private Reader getReaderForCountryFile() {
		String fileName = getCountryFileName();
		InputStream in = getInputStream(fileName);
		if (in == null) {
			String folder = OUTER_FOLDER;
			LOG.info("File not found : " + folder + fileName);
			fileName = DEFAULT_JSON_FILE;
			folder = INNER_FOLDER;
			LOG.info("Try to load : " + folder + fileName);
			// Search for default JSON file
			in = getClass().getResourceAsStream(folder + fileName);
		}
		return new InputStreamReader(in, StandardCharsets.UTF_8);
	}

	private Reader getReaderForCountrySubdivisionFile() {
		if (this.countrySubdivisionCode == null) {
			return null;
		}
		String fileName = getCountrySubdivisionFileName();
		InputStream in = getInputStream(fileName);
		if (in == null) {
			return null;
		}
		return new InputStreamReader(in, StandardCharsets.UTF_8);
	}

	private InputStream getInputStream(String fileName) {
		// First search in external folder
		String folder = OUTER_FOLDER;
		InputStream in = getClass().getResourceAsStream(folder + fileName);
		if (in == null) {
			LOG.info("File not found : " + folder + fileName);
			folder = INNER_FOLDER;
			LOG.info("Try to load : " + folder + fileName);
			// Search in sub-package "conf" of the jar
			in = getClass().getResourceAsStream(folder + fileName);
		}
		if(in != null) {
			LOG.info("File used : " + folder+fileName);
		}
		return in;
	}

	private void initializePublicHolidays(PublicHolidays ph, Reader reader) throws ParseException, IOException, PublicHolidayException {
		JSONParser parser = new JSONParser();
		JSONObject object = (JSONObject) parser.parse(reader);
		JSONArray array = (JSONArray) object.get("fixed");
		initializeFixedHolidays(ph, array);
		array = (JSONArray) object.get("mobile");
		initializeMobileHolidays(ph, array);
	}

	private void initializeFixedHolidays(PublicHolidays ph, JSONArray array) throws ParseException, IOException {
		if (array == null) {
			return;
		}
		Iterator<JSONObject> it = array.iterator();
		while (it.hasNext()) {
			JSONObject obj = it.next();
			int day = ((Long) obj.get("day")).intValue();
			int month = ((Long) obj.get("month")).intValue();
			String name = (String) obj.get("name");
			PublicHoliday holiday = create(name, month, day);
			ph.add(holiday);
		}
	}

	private void initializeMobileHolidays(PublicHolidays ph, JSONArray array) throws ParseException, IOException, PublicHolidayException {
		if (array == null) {
			return;
		}
		Iterator<JSONObject> it = array.iterator();
		while (it.hasNext()) {
			JSONObject obj = it.next();
			String name = (String) obj.get("name");
			String base = ((String) obj.get("base"));
			int offset = ((Long) obj.get("offset")).intValue();

			BaseDateCompute baseDateCompute = BaseDateComputeFactory.getInstance().create(base);
			PublicHoliday holiday = create(name, baseDateCompute.getHolidayDate(year, offset));
			ph.add(holiday);
		}
	}

	private PublicHoliday create(String name, Calendar date) {
		return new PublicHoliday(date, name);
	}

	private PublicHoliday create(String name, int month, int day) {
		GregorianCalendar gc = new GregorianCalendar(year, month - 1, day);
		PublicHoliday holiday = new PublicHoliday(gc, name);
		return holiday;
	}

	private String getCountryFileName() {
		StringBuffer fileName = new StringBuffer();
		fileName.append(FILE_NAME).append('-').append(countryCode);
		fileName.append('.').append(FILE_EXTENSION);
		return fileName.toString();
	}

	private String getCountrySubdivisionFileName() {
		StringBuffer fileName = new StringBuffer();
		fileName.append(FILE_NAME).append('-').append(countryCode).append('-').append(countrySubdivisionCode);
		fileName.append('.').append(FILE_EXTENSION);
		return fileName.toString();
	}

	private void initialize() {
		if (!yearSetted) {
			Calendar cal = new GregorianCalendar();
			this.year = cal.get(Calendar.YEAR);
		}
		if (countryCode == null) {
			countryCode = "FR";
		}
	}

}
