package org.antislashn.holidays;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
/**
 * Collection of PublicHoliday returned by the builder PublicHolidaysBuilder
 * <br>
 * This collection is Iterable
 * 
 * @author Franck SIMON
 *
 */
public class PublicHolidays implements Serializable, Iterable<PublicHoliday>{
	private List<PublicHoliday> holidays = new ArrayList<>();
	private PublicHolidayComparator comparator = new PublicHolidayComparator();
	
	public void add(PublicHoliday publicHoliday) {
		this.holidays.add(publicHoliday);
		Collections.sort(this.holidays, comparator);
	}

	@Override
	public Iterator<PublicHoliday> iterator() {
		return this.holidays.iterator();
	}
	
	private class PublicHolidayComparator implements Comparator<PublicHoliday>{

		@Override
		public int compare(PublicHoliday ph1, PublicHoliday ph2) {
			return ph1.getDate().compareTo(ph2.getDate());
		}
		
	}
}
